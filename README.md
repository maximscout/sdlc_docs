Congratulations on finishing the Software Development Lifecycle course with SOA, JMS, GIT, and more.

Below are the list of ebooks for you to continue your education and learning.

Enjoy and look for updates of versions.

* ActiveMQ in Action.pdf
* Apache Tomcat 7.pdf
* Apache_Tomcat7-User_Guide.pdf
* Eclipse.pdf
* Eclipse IDE Pocket Guide.pdf
* Jenkins Continuous Integration Cookbook, 2nd Edition.pdf
* jenkins-the-definitive-guide.pdf
* junit_tutorial.pdf
* just_spring.pdf
* log4j-users-guide.pdf
* Mastering Apache Maven 3.pdf
* Pragmatic Unit Testing in Java 8 with JUnit.pdf
* Pragmatic Unit Testing in Java with JUnit.pdf
* Spring in Action, 3rd Edition.pdf
* Spring in Action, 4th Edition.pdf
* Tomcat_ The Definitive Guide, 2nd Edition.pdf


Keep on Learning!!!

Eric